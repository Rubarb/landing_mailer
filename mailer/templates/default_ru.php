<?php
/**
 * Default russian e-mail template
 * Created by MactepDark
 * @var $this Sender
 * @var $data array
 * @var $url string
 * @var $title string
 * @var $form string
 * @var $button string
 */
?>
<h3>Заявка</h3>
На этот адрес была отправлена заявка с сайта <a href="<?= $this->detail->url ?>"><?= $this->detail->title ?></a>,<br/>
используя форму "<?= $this->detail->form ?>" по нажатию на кнопку <?= $this->detail->button ?><br/>
<hr width="95%" size="1" noshade />
<dl>
    <?php foreach ($this->post as $key => $value): ?>
        <dt>
            <?= Sender::getFieldName($key) ?>
        </dt>
        <dd>
            <?= $value ?>
        </dd>
    <?php endforeach; ?>
</dl>

<small>
    &copy; <?= date('Y') ?> <a href="https://rubarbs.com">
        <img src="https://rubarbs.com/rubarb_digital.png" alt="RUBARB Digital" />
    </a>
</small>
