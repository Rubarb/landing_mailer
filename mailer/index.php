<?php
require_once ('PHPMailer/class.phpmailer.php');
$app = new Sender();
die($app->pushMsg());

class Sender {

    const CONFIG_FILENAME = 'config.json';
    const METHOD_NAME = 'pushMsg';

    private $config;
    private $result = ['error' => false, 'result' => false];
    private $startTime;
    private $post;
    private $file = false;
    private $detail;
    private static $fields = [];
    private $email;

    function __construct() {
        $this->startTime = microtime(true);
        $this->config = json_decode(file_get_contents(self::CONFIG_FILENAME));
        self::$fields = (array)$this->config->Fields;
        $this->parsePost($_POST);
        if (count($_FILES)) { $this->file = $_FILES['file']; }
        $this->email = new PHPMailer();
        $this->email->CharSet = $this->config->Mail->charset;
        $this->email->From = $this->config->Mail->from.'@'.$_SERVER['HTTP_HOST'];
        $this->email->FromName = $this->detail->title;
        $this->email->Subject = $this->config->Mail->subject.' '.$_SERVER['HTTP_HOST'];
    }

    public function pushMsg() {
        ob_start();
        require_once ('templates/default_ru.php');
        $this->email->Body = ob_get_clean();
        $this->email->isHTML(true);
        foreach ($this->config->Mail->to as $address) { $this->email->addAddress($address); }
        if ($this->file) { $this->email->addAttachment($this->file['tmp_name'], $this->file['name']); }
        $this->result['result'] = $this->email->send();
        $this->result['config'] = $this->config;
        $this->result['time'] = number_format(microtime(true) - $this->startTime, 5);
        return json_encode($this->result);
    }

    private function parsePost($post) {
        if (count($post)) {
            if (isset($post['method'])) {
                if ($post['method'] == self::METHOD_NAME) {
                    unset($post['method']);
                    $this->detail = new stdClass();
                    $this->detail->url = $post['url']; unset($post['url']);
                    $this->detail->title = $post['title']; unset($post['title']);
                    $this->detail->form = $post['form']; unset($post['form']);
                    $this->detail->button = $post['button']; unset($post['button']);
                    $this->post = $post;
                } else { $this->result['error'] = 'Unknown method!'; }
            } else { $this->result['error'] = 'No method is specified!'; }
        } else { $this->result['error'] = 'Empty request!'; }
    }

    public static function getFieldName($field) {
        return (array_key_exists($field, self::$fields)) ? self::$fields[$field] : $field;
    }
}