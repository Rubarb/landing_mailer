$(document).on('click', 'button.form-submit', function(e) {
    e.preventDefault();
    var formName = $(this).prop('id').split('-submit')[0];
    var fieldsClass = formName + '-field';
    var elements = $('.' + fieldsClass);
    var data = parseElements(elements);
    // console.log(data); return;
    if (data != false) {
        var form = new FormData();
        $.each(data, function(k, v) { form.append(k, v); });
        form.append('method', 'pushMsg');
        form.append('button', $(this).html());
        form.append('form', $('form#'+formName).attr('name'));
        form.append('url', location.href);
        form.append('title', document.title);
        $('div#'+formName+'-message').html('Отправка...');
        $.ajax({
            url: 'mailer/index.php',
            type: 'POST',
            data: form,
            contentType: false,
            processData: false,
            success: function(ans) {
                var answer = JSON.parse(ans);
                if (answer.result) {
                    $('div#'+formName+'-message').html('Сообщение успешно отправлено!');
                } else {
                    console.log(answer);
                    alert(answer.error);
                }
            }
        });
    } else { return false; }
});

function parseElements(elements) {
    var data = {};
    var validated = true;
    var str = [];
    $.each(elements, function(k, v) {
        if ($(v).prop('required') && $(v).val().length == 0) {
            $(v).addClass('required').focus();
            validated = false;
            return false;
        } else { $(v).removeClass('required'); }
        switch (v.nodeName) {
            case 'INPUT': switch ($(v).prop('type')) {
                case 'text': data[$(v).prop('name')] = $(v).val();
                    break;
                case 'checkbox': if ($(v).prop('checked')) {
                    str.push($(v).prop('name'));
                    data[$(v).attr('datafld')] = str.join(', ');
                }
                    break;
                case 'radio': if ($(v).prop('checked')) { data[$(v).prop('name')] = $(v).val(); }
                    break;
                case 'file': if ($(v).length) { data[$(v).prop('name')] = $(v)[0].files[0]; }
                    break;
                default: break;
            }
                break;
            case 'SELECT': data[$(v).prop('name')] = $(v).val();
                break;
        }
    });
    return validated ? data : validated;
}